#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTR 255

char alfabeto [] = "abcdefghijklmnopqrstuvxywz";

int posicao(char letra){
	int x = 0;
	for (x = 0; x <= 26; x++){
		if(alfabeto[x] == letra){
			return x;
		}
	}
}

// n é 26, portanto essa é uma função m = x mod n
int mod(int m){
	if(m<=26 && m>=0){
		return m;
	}
	if(m<0){
                return 26+m;
        }
	else{
		return m%26;
	}
}

int menu(){
	int escolha;
	printf("Bem vindo ao deccod!\n");
	printf("1 para criptografar\n");
	printf("2 para descriptografar\n");
	printf("3 para finalizar programa\n");
	printf("DIGITE A SUA ESCOLHA>>");
	scanf("%d", &escolha);
	return escolha;
}

int main(){
	char chave[200], nome_arq[50], palavra[MAXSTR];
	int n, escolha = 0;
	FILE *arquivo;
	while(1){
		printf("\n");
		escolha = menu();

		//Finaliza o programa
		if(escolha == 3){return 0;}

		printf("Digite uma chave>> ");
		scanf("%s", chave);

		printf("Digite um número>> ");
		scanf("%d", &n);
	
		printf("Digite o nome do arquivo a ser utilizado>> ");
		scanf("%s", nome_arq);
	
		int i = 0, q = strlen(chave), x = 0;

		char *msgCript;
		char *msgDescript;

		msgCript = (char*)malloc(MAXSTR);
		msgDescript = (char*)malloc(MAXSTR);	

		switch(escolha){
			case 1:
				printf("Digite uma palavra>>");
				scanf("%s", palavra);
				palavra[MAXSTR-1] = '\0';
				arquivo = fopen(nome_arq, "w");
				// A cada interacao um caracter cript. é gerado e adicionado a msg criptografada
				for(x=0; x<strlen(palavra); x++){
				//printf("l = %c\t| pl = %d | c =%c\t| pc = %d\t", palavra[x], posicao(palavra[x]), chave[i], posicao(chave[i]));
					msgCript[x] = alfabeto[mod(posicao(chave[i])+(n+x)+posicao(palavra[x]))];
					if(i == q-1){
						i = 0;
					}
					else{
						i++;
					}
				}			
				fprintf(arquivo,"%s", msgCript);
				printf("\nA msg criptografada é %s\n", msgCript);
				fclose(arquivo);
				free(msgCript);
				free(msgDescript);	
				break;	
			case 2:
				arquivo = fopen(nome_arq, "r");
				fscanf(arquivo,"%s", msgCript);
				for(x = 0; x<strlen(msgCript); x++){
						msgDescript[x] = alfabeto[mod(posicao(msgCript[x])-posicao(chave[i])-(n+x))];
						if(i == q-1){
							i = 0;
						}
						else{
							i++;
						}	
				}
				printf("Decodificada é %s\n", msgDescript);
				fclose(arquivo);
				free(msgCript);
				free(msgDescript);	
				break;
		}
	}
	return 0;
}
